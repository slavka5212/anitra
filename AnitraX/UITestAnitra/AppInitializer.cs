﻿using System;
using System.IO;
using System.Reflection;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITestAnitra
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                return ConfigureApp.Android
                    .InstalledApp("com.companyname.AnitraX")
                    .PreferIdeSettings()
                    .EnableLocalScreenshots()
                    
                    .StartApp();
            }

            return null; // ConfigureApp.iOS.EnableLocalScreenshots().StartApp();
        }
    }
}