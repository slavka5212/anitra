﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Widget;
using AnitraX.MapCS;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using System.Collections.ObjectModel;
using Android.Gms.Maps.Model;
using Android.Gms.Maps;
using AnitraX.Models;
using System.Collections.Specialized;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using Android.Views;
using Android.Icu.Text;
using Android.Graphics.Drawables;
using Android.Graphics;
using Xamarin.Forms.Internals;

[assembly: ExportRenderer(typeof(CustomMap), typeof(AnitraX.Droid.CustomMapRenderer))]
namespace AnitraX.Droid
{
    public class CustomMapRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter
    {
        IList<ExtendedPin> customPins;
        IList<Position> routeCoordinates;
        IList<TrackObject> tracksCollection;
        List<Marker> _markers;
        List<Circle> _circles = new List<Circle>();
        List<Polyline> _polylines;
        private bool _mapDrawn;
        private CustomMap _formsMap;

        protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);
            
            if (e.OldElement != null)
            {
                _formsMap = (CustomMap)e.OldElement;
                tracksCollection = _formsMap.TracksCollection;
                ((ObservableCollection<TrackObject>)tracksCollection).CollectionChanged -= OnTracksChanged;
                routeCoordinates = _formsMap.RouteCoordinates;
                ((ObservableCollection<Position>)routeCoordinates).CollectionChanged -= OnRoutesChanged;
                customPins = _formsMap.Items;
                ((ObservableCollection<ExtendedPin>)customPins).CollectionChanged -= OnCollectionChanged;
                NativeMap.InfoWindowClick -= MapOnInfoWindowClick;
            }

            if (e.NewElement != null)
            {
                _formsMap = (CustomMap)e.NewElement;
                tracksCollection = _formsMap.TracksCollection;
                ((ObservableCollection<TrackObject>)tracksCollection).CollectionChanged += OnTracksChanged;
                routeCoordinates = _formsMap.RouteCoordinates;
                ((ObservableCollection<Position>)routeCoordinates).CollectionChanged += OnRoutesChanged;
                customPins = _formsMap.Items;
                ((ObservableCollection<ExtendedPin>)customPins).CollectionChanged += OnCollectionChanged;
            }
            Control.GetMapAsync(this);
        }
        /*
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == CustomMap.RouteCoordinatesProperty.PropertyName && _formsMap.RouteCoordinates.Count > 1)
            {
                GoogleMap map = NativeMap;
                if (map == null)
                {
                    return;
                }

                var polylineOptions = new PolylineOptions();
                polylineOptions.InvokeColor(0x66FF0000);
                polylineOptions.InvokeWidth(5f);

                foreach (var position in _formsMap.RouteCoordinates)
                {
                    polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));
                }
                var polyline = map.AddPolyline(polylineOptions);
                _polylines.Add(polyline);
            }
        }*/

        /// <summary>
        /// Shows items and routes on the map.
        /// To view given objects when loading a map.
        /// </summary>
        private void ShowMapItemsRoutes()
        {
            GoogleMap map = NativeMap;
            if (map == null)
            {
                return;
            }

            if (_polylines != null)
            {
                foreach (Polyline line in _polylines)
                {
                    line.Visible = false;
                    line.Remove();
                }
            }
            _polylines = new List<Polyline>();

            if ( _formsMap.RouteCoordinates.Count > 1)
            {
                var polylineOptions = new PolylineOptions();
                polylineOptions.InvokeColor(0x66FF0000);
                polylineOptions.InvokeWidth(5f);

                foreach (var position in _formsMap.RouteCoordinates)
                {
                    polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));

                    var circleOpt = CreateCircle(position);
                    var circle = map.AddCircle(circleOpt);
                    _circles.Add(circle);
                }
                var polyline = map.AddPolyline(polylineOptions);
                _polylines.Add(polyline);
            }
            if (_formsMap.Items.Count > 0)
            {
                AddPins(_formsMap.Items);
            }
            if (_formsMap.TracksCollection.Count > 0)
            {
                foreach (var trackObject in _formsMap.TracksCollection)
                {
                    var polylineOptions = new PolylineOptions();

                    foreach (Location location in trackObject.MapData)
                    {
                        polylineOptions.Add(new LatLng(location.Latitude, location.Longitude));

                        var circleOpt = CreateCircle(location.getPosition(), trackObject.Color);
                        var circle = map.AddCircle(circleOpt);
                        _circles.Add(circle);
                    }
                    polylineOptions.InvokeColor(Android.Graphics.Color.ParseColor(trackObject.Color));
                    polylineOptions.InvokeWidth(5f);

                    var polyline = map.AddPolyline(polylineOptions);
                    _polylines.Add(polyline);
                }
            }
        }

        private void OnRoutesChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (routeCoordinates.Count > 1)
            {
                GoogleMap map = NativeMap;
                if (map == null)
                {
                    return;
                }
                routeCoordinates = _formsMap.RouteCoordinates;

                var polylineOptions = new PolylineOptions();
                polylineOptions.InvokeColor(0x66FF0000);
                polylineOptions.InvokeWidth(5f);

                foreach (var position in routeCoordinates)
                {
                    polylineOptions.Add(new LatLng(position.Latitude, position.Longitude));
                }
                var polyline = map.AddPolyline(polylineOptions);
                _polylines.Add(polyline);
            }
        }

        private void OnTracksChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (tracksCollection.Count > 0)
            {
                GoogleMap map = NativeMap;
                if (map == null)
                {
                    return;
                }

                if (_polylines != null)
                {
                    foreach (Polyline line in _polylines)
                    {
                        line.Visible = false;
                        line.Remove();
                    }
                }
                _polylines = new List<Polyline>();

                tracksCollection = _formsMap.TracksCollection;

                foreach (var trackObject in tracksCollection)
                {
                    var polylineOptions = new PolylineOptions();

                    foreach (Location location in trackObject.MapData)
                    {
                        polylineOptions.Add(new LatLng(location.Latitude, location.Longitude));

                        var circleOpt = CreateCircle(location.getPosition(), trackObject.Color);
                        var circle = map.AddCircle(circleOpt);
                        _circles.Add(circle);
                    }
                    polylineOptions.InvokeColor(Android.Graphics.Color.ParseColor(trackObject.Color));
                    polylineOptions.InvokeWidth(5f);

                    var polyline = map.AddPolyline(polylineOptions);
                    _polylines.Add(polyline);
                }
            }
        }

        void OnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    AddPins(e.NewItems);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    RemovePins(e.OldItems);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    RemovePins(e.OldItems);
                    AddPins(e.NewItems);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    _markers?.ForEach(m => m.Remove());
                    _markers = null;
                    _circles?.ForEach(m => m.Remove());
                    AddPins((IList)customPins);
                    break;
                case NotifyCollectionChangedAction.Move:
                    //do nothing
                    break;
            }
        }
        void AddPins(IList pins)
        {
            GoogleMap map = NativeMap;
            if (map == null)
            {
                return;
            }

            if (_markers == null)
            {
                _markers = new List<Marker>();
            }

            _markers.AddRange(pins.Cast<ExtendedPin>().Select(p =>
            {
                ExtendedPin pin = p;
                var opts = CreateMarker(pin);
                var marker = map.AddMarker(opts);
                
                var circleOpts = CreateCircle(pin.Position, pin.PinColor);
                var circle = map.AddCircle(circleOpts);
                _circles.Add(circle);

                pin.PropertyChanged += PinOnPropertyChanged;

                // associate pin with marker for later lookup in event handlers
                pin.MarkerId = marker.Id;
                // associate pin with circle for later lookup in event handlers
                pin.CircleId = circle.Id;
                return marker;
            }));

        }

        void RemovePins(IList pins)
        {
            GoogleMap map = NativeMap;
            if (map == null)
            {
                return;
            }
            if (_markers == null)
            {
                return;
            }

            foreach (ExtendedPin p in pins)
            {
                p.PropertyChanged -= PinOnPropertyChanged;

                var circle = GetCircleForPin(p);
                if (circle != null)
                {
                    circle.Remove();
                    _circles.Remove(circle);
                }

                var marker = GetMarkerForPin(p);
                if (marker != null)
                {
                    marker.Remove();
                    _markers.Remove(marker);
                }
            }
            
        }

        void PinOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ExtendedPin pin = (ExtendedPin)sender;
            Marker marker = GetMarkerForPin(pin);

            if (marker == null)
            {
                return;
            }

            if (e.PropertyName == ExtendedPin.LabelProperty.PropertyName)
            {
                marker.Title = pin.Label;
            }
            else if (e.PropertyName == ExtendedPin.AddressProperty.PropertyName)
            {
                marker.Snippet = pin.Address;
            }
            else if (e.PropertyName == ExtendedPin.PositionProperty.PropertyName)
            {
                marker.Position = new LatLng(pin.Position.Latitude, pin.Position.Longitude);
            }
        }
        protected Marker GetMarkerForPin(ExtendedPin pin)
        {
            return _markers?.Find(m => m.Id == (string)pin.MarkerId);
        }

        protected Circle GetCircleForPin(ExtendedPin pin)
        {
            return _circles?.Find(c => c.Id == (string)pin.CircleId);
        }

        void MapOnMarkerClick(object sender, GoogleMap.InfoWindowClickEventArgs eventArgs)
        {
            // clicked marker
            var marker = eventArgs.Marker;
            GoogleMap map = NativeMap;

            // lookup pin
            ExtendedPin targetPin = null;
            for (var i = 0; i < customPins.Count; i++)
            {
                ExtendedPin pin = customPins[i];
                if ((string)pin.MarkerId != marker.Id)
                {
                    continue;
                }

                targetPin = pin;
                break;
            }

            _formsMap.SelectedPin = targetPin;
            // only consider event handled if a handler is present.
            // Else allow default behavior of displaying an info window.
            targetPin?.SendTap();
        }

        private static int GetPinIcon()
        {
            return Resource.Drawable.anitra_logo_crop;
        }

        private void HandleMarkerClick(object sender, GoogleMap.MarkerClickEventArgs e)
        {
            var marker = e.Marker;

            // lookup pin
            ExtendedPin targetPin = null;
            for (var i = 0; i < customPins.Count; i++)
            {
                ExtendedPin pin = customPins[i];
                if ((string)pin.MarkerId != marker.Id)
                {
                    continue;
                }

                targetPin = pin;
                break;
            }

            var formsMap = (CustomMap)Element;
            formsMap.SelectedPin = targetPin;

            marker.ShowInfoWindow();
        }

        void MapOnInfoWindowClick(object sender, GoogleMap.InfoWindowClickEventArgs e)
        {
            var formsMap = (CustomMap)Element;
            if (formsMap.SelectedPin != null && formsMap.SelectedPin.TrackedObjectId != null)
            {
                formsMap.ShowDetailCommand.Execute(formsMap.SelectedPin);
            }
        }

        private bool IsItem(IMapModel item, Marker marker)
        {
            return item.Name == marker.Title &&
                   item.Details == marker.Snippet &&
                   item.Location.Latitude == marker.Position.Latitude &&
                   item.Location.Longitude == marker.Position.Longitude;
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            //NOTIFY CHANGE

            if (changed)
            {
            }
           
        }

        public CustomMapRenderer(Context context) : base(context)
        {
            
        }

        /// <summary>
        /// Overrides the CreateMarker from the Pin
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        protected override MarkerOptions CreateMarker(Pin pin)
        {
            var marker = new MarkerOptions();
            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            marker.SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueBlue));
            
            //marker.SetAlpha(BitmapDescriptorFactory.HueYellow);
            //marker.SetIcon(BitmapDescriptorFactory.FromResource(Resource.Drawable.pin));

            return marker;
        }

        /// <summary>
        /// CreateMarker creates a Marker from the ExtendedPin
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        protected MarkerOptions CreateMarker(ExtendedPin pin)
        {
            BitmapDescriptor bd = (BitmapDescriptorFactory.FromResource(Resource.Drawable.dot_gray));
            var marker = new MarkerOptions();

            marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
            marker.SetTitle(pin.Label);
            marker.SetSnippet(pin.Address);
            marker.SetIcon(bd);
            marker.Anchor(0.5f, 0.5f);

            // marker.SetIcon(BitmapDescriptorFactory.DefaultMarker(pin.PinColor));

            return marker;
        }

        /// <summary>
        /// CreateCircle creates a Circle from the Position
        /// </summary>
        /// <param name="position"></param>
        /// <param name="colorString"></param>
        /// <returns></returns>
        protected CircleOptions CreateCircle(Position position, string colorString = "#e6d9534f")
        {
            var circle = new CircleOptions();
            var color = Android.Graphics.Color.ParseColor(colorString);

            circle.InvokeCenter(new LatLng(position.Latitude, position.Longitude));
            circle.InvokeRadius(5);
            circle.InvokeStrokeWidth(1);
            circle.InvokeStrokeColor(color);

            color.A = 80;
            circle.InvokeFillColor(color);

            return circle;
        }

        protected override void OnMapReady(GoogleMap googleMap)
        {
            if (_mapDrawn) return;

            base.OnMapReady(googleMap);

            NativeMap.MarkerClick += HandleMarkerClick;
            NativeMap.InfoWindowClick += MapOnInfoWindowClick;
            NativeMap.SetInfoWindowAdapter(this);
            
            if (_formsMap.IsShowingUser)
            {
                NativeMap.MyLocationEnabled = _formsMap.IsShowingUser;
                googleMap.UiSettings.MyLocationButtonEnabled = true;
                googleMap.MyLocationEnabled = true;
            }
            
            this.ShowMapItemsRoutes();

            _mapDrawn = true;
        }
        
        public Android.Views.View GetInfoContents(Marker marker)
        {
            var inflater = Android.App.Application.Context.GetSystemService(Context.LayoutInflaterService) as Android.Views.LayoutInflater;
            if (inflater != null)
            {
                Android.Views.View view;

                view = inflater.Inflate(Resource.Layout.MapInfoWindow, null);
                TextView infoTitle = view.FindViewById<TextView>(Resource.Id.InfoWindowTitle);
                TextView infoSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSubtitle);
                TextView infoSecondSubtitle = view.FindViewById<TextView>(Resource.Id.InfoWindowSecondSubtitle);
                TextView infoDateTime = view.FindViewById<TextView>(Resource.Id.InfoWindowDateTimeText);
                TextView infoLocation = view.FindViewById<TextView>(Resource.Id.InfoWindowLocationText);
                TextView infoAltitude = view.FindViewById<TextView>(Resource.Id.InfoWindowAltitudeText);

                infoTitle.Text = marker.Title;
                infoSubtitle.Text = marker.Snippet;
                

                var pin = _formsMap.SelectedPin;
                if (pin != null)
                {
                    if (pin.Name != null && pin.Name != "")
                        infoTitle.Text = pin.Name;
                    else
                    {
                        infoTitle.Visibility = ViewStates.Gone;
                        view.FindViewById<Android.Views.View>(Resource.Id.InfoWindowSeparator).Visibility = ViewStates.Gone;
                    }

                    if (pin.DeviceInfo != null)
                        infoSubtitle.Text = pin.DeviceInfo;
                    else
                        infoSubtitle.Visibility = ViewStates.Gone;

                    if (pin.SpeciesInfo != null)
                        infoSecondSubtitle.Text = pin.SpeciesInfo;
                    else
                        infoSecondSubtitle.Visibility = ViewStates.Gone;

                    infoDateTime.Text = pin.Location.getDateTimeString();

                    infoLocation.Text = String.Format("{0:F6} {1:F6}", pin.Location.Latitude, pin.Location.Longitude);

                    if (pin.Location.Altitude != 0)
                    {
                        infoAltitude.Text = pin.Location.getAltitudeString();
                    } else
                    {
                        view.FindViewById<Android.Views.View>(Resource.Id.InfoWindowSecondSeparator).Visibility = ViewStates.Gone;
                        infoAltitude.Visibility = ViewStates.Gone;
                    }
                }
                
                return view;
            }
            return null;
        }

        public Android.Views.View GetInfoWindow(Marker marker)
        {
            //it will use the default window
            return null;
        }
    }
}

