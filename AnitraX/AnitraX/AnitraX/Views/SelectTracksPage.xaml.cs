﻿using AnitraX.Models;
using AnitraX.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectTracksPage : ContentPage
    {
        TracksViewModel viewModel;

        public SelectTracksPage()
        {
            InitializeComponent();
        }

        public SelectTracksPage(TracksViewModel viewModel)
        {
            BindingContext = this.viewModel = viewModel;

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Routes.Count == 0)
                viewModel.LoadRoutesCommand.Execute(null);

            MessagingCenter.Subscribe<string>(this, "AddRoute", (arg) =>
            {
                // refresh
                InitializeComponent();
            });
        }

        private void SelectRouteCommand(int toId)
        {
            foreach (Route item in viewModel.Routes)
            {
                if (item.Id == toId)
                    item.Selected = !item.Selected;
            }
        }
    }
}