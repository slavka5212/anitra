﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static AnitraX.Data.UserManager;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
            // Constants used for development
            emailEntry.Text = Constants.Email;
            passwordEntry.Text = Constants.Password;

            login.Clicked += (sender, e) => {
                Login(emailEntry.Text, passwordEntry.Text);
            };
        }

        // Makes a call to the REST API and attempts to authenticate the user.
        public bool Login(string email, string password)
        {
            // Create the request and add the necessary parameters, used the RestSharp library
            var client = new RestClient("https://anitra.cz/demo/api/v1/login");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("email", email);
            request.AddParameter("password", password);
         
            IRestResponse response = client.Execute(request);
            
            // Deserialize the string into an object with the Newtonsoft.Json library
            // UserPostResponse class captures the api_key
            UserPostResponse items = JsonConvert.DeserializeObject<UserPostResponse>(response.Content);
            
            string token = items.API_KEY;
            if (token != null)
            {
                Application.Current.Properties["access_token"] = token;
                Application.Current.Properties["is_logged_in"] = Boolean.TrueString;
                Application.Current.MainPage = new MainPage();

                // DisplayAlert("Login", token, "OK");
                return true;
            }
            else
            {
                DisplayAlert("Login failed", "Incorrect email or password. Please try again.", "OK");
                return false;
            };
        }
    }
}