﻿using AnitraX.Models;
using AnitraX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TOsPage : ContentPage
	{
        TrackedObjectsViewModel viewModel;

        public TOsPage ()
		{
			InitializeComponent();

            BindingContext = viewModel = new TrackedObjectsViewModel();

            SearchBar.TextChanged += SearchBar_OnTextChanged;
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as TrackedObject;
            if (item == null)
                return;

            await Navigation.PushAsync(new TODetailPage(new TODetailViewModel(item)));

            // Manually deselect item.
            TOsListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            if (viewModel.TrackedObjects.Count == 0)
                viewModel.LoadTrackedObjectsCommand.Execute(null);
        }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            TOsListView.BeginRefresh();
            var searchText = e.NewTextValue.ToLower();
            
            if (string.IsNullOrWhiteSpace(e.NewTextValue))
                TOsListView.ItemsSource = viewModel.TrackedObjects;
            else
                TOsListView.ItemsSource = viewModel.TrackedObjects.Where(i => 
                    i.TrackedObjectName.ToLower().Contains(searchText) ||
                    i.TrackedObjectCode.ToLower().Contains(searchText));

            TOsListView.EndRefresh();
        }
    }
}