﻿using AnitraX.MapCS;
using AnitraX.Models;
using AnitraX.Resources;
using AnitraX.ViewModels;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using GeoPosition = Plugin.Geolocator.Abstractions.Position;
using Position = Xamarin.Forms.Maps.Position;
using System.Threading;
using System.Threading.Tasks;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        private static String[] MAP_TYPE_ITEMS = { "Street", "Hybrid", "Satellite"};

        bool tracking;
        int updatesTracking;
        public ObservableCollection<Location> Positions { get; } = new ObservableCollection<Location>();
        private MapObject MyLastPosition;
        private int GPSdataCollectionSec = Constants.MinSecGPSdataCollection;


        public TrackedObjectsViewModel TOsViewModel { get; set; }
        public TracksViewModel RoutesViewModel { get; set; }
        public MapViewModel ViewModel { get { return BindingContext as MapViewModel; } }
        

        public MapPage ()
		{
            TOsViewModel = new TrackedObjectsViewModel();
            RoutesViewModel = new TracksViewModel();
            BindingContext = new MapViewModel();

            InitializeComponent ();

            Title = "Anitra";
            //loadButton.Text = AppResources.LoadButtonText;

            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(50.089217586, 14.459573379), Distance.FromMiles(1.0)));
            foreach (string typeName in MAP_TYPE_ITEMS)
            {
                typePicker.Items.Add(typeName);
            }

            typePicker.SelectedIndex = 0;
            typePicker.SelectedIndexChanged += typePickerSelectedIndexChanged;

            customMap.ShowDetailCommand = ShowDetailCommand;

            ViewModel.MapObjects.CollectionChanged += UpdatePins;
            
        }
        
        private void SelectTOsButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SelectTOsPage(TOsViewModel));
        }

        private void SelectTracksButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SelectTracksPage(RoutesViewModel));
        }

        private async void TrackingButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                var hasPermission = await Utils.CheckPermissions(Permission.Location);
                /*if (!hasPermission)
                    return;*/

                if (tracking)
                {
                    CrossGeolocator.Current.PositionChanged -= CrossGeolocator_Current_PositionChanged;
                    CrossGeolocator.Current.PositionError -= CrossGeolocator_Current_PositionError;
                }
                else
                {
                    CrossGeolocator.Current.PositionChanged += CrossGeolocator_Current_PositionChanged;
                    CrossGeolocator.Current.PositionError += CrossGeolocator_Current_PositionError;
                }

                if (CrossGeolocator.Current.IsListening)
                {
                    await CrossGeolocator.Current.StopListeningAsync();
                    bool save = await DisplayAlert("Tracking stopped", "Would you like to save your track?", "Save", "No");
                    if (save)
                    {
                        Route route = new Route
                        {
                            Note = "",
                            Locations = Positions.ToList(),
                            Time = DateTimeOffset.Now.ToUnixTimeSeconds()
                        };
                        
                        await App.DB.AddRouteAsync(route);

                        var locations = Positions.ToList();
                        foreach (var l in locations)
                        {
                            l.RouteId = route.Id;
                        }
                        route.Name = route.Id.ToString();

                        Positions.Clear();
                        await App.DB.AddLocationsAsync(locations);

                        route.Locations = locations;
                        await App.DB.StoreRouteAsync(route);

                        Route r = await App.DB.GetRouteAsync(route.Id);
                        if (r != null)
                        {
                            await DisplayAlert("Done", "Track saved", "Ok");

                            MessagingCenter.Send(this, "AddRoute", route.Id);
                            await Navigation.PushAsync(new RouteEditPage(new RouteDetailViewModel(r)));
                        }
                        else
                            await DisplayAlert("Error", "Track not saved", "Ok");
                    }
                    trackingButton.Text = "Start Tracking";
                    tracking = false;
                    updatesTracking = 0;
                }
                else
                {
                    Positions.Clear();

                    if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(GPSdataCollectionSec), 10, true, new Plugin.Geolocator.Abstractions.ListenerSettings
                    {
                        ActivityType = Plugin.Geolocator.Abstractions.ActivityType.AutomotiveNavigation,
                        AllowBackgroundUpdates = true,
                        DeferLocationUpdates = true,
                        DeferralDistanceMeters = 1,
                        DeferralTime = TimeSpan.FromSeconds(1),
                        ListenForSignificantChanges = true,
                        PauseLocationUpdatesAutomatically = false
                    }))
                       {
                        trackingButton.Text = "Stop Tracking";
                        tracking = true;
                       } 
                }
            }
            catch (Exception)
            {
                await DisplayAlert("Uh oh", "Something went wrong.", "OK");
            }
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            
            GPSdataCollectionSec = Application.Current.Properties.ContainsKey("gps_data_sec") ? Convert.ToInt32(Application.Current.Properties["gps_data_sec"]) : Constants.MinSecGPSdataCollection;

            if (TOsViewModel.TrackedObjects.Count == 0)
                TOsViewModel.LoadTrackedObjectsCommand.Execute(null);

            if (RoutesViewModel.Routes.Count == 0)
                RoutesViewModel.LoadRoutesCommand.Execute(null);

            bool hasPermission = await Utils.CheckPermissions(Permission.Location);
            if (hasPermission)
            {
                customMap.IsShowingUser = true;
            }
            showSelectedTOs();
        }

        /// <summary>
        /// Show selected Tracked Objects and Routes
        /// </summary>
        private void showSelectedTOs()
        {
            ViewModel.MapObjects.Clear();
            customMap.TracksCollection.Clear();

            int colorIndex = 0;
            foreach (TrackedObject to in TOsViewModel.TrackedObjects)
            {
                if (to.Selected)
                {
                    customMap.TracksCollection.Add(
                        new TrackObject() { MapData = to.MapData, Color = MapObject.Colors[colorIndex] }
                    );

                    
                    foreach (Location d in to.MapData)
                    {
                        ViewModel.MapObjects.Add( 
                            new MapObject
                            {
                                Name = to.TrackedObjectCodeAndName,
                                DeviceInfo = to.DeviceCodeString,
                                SpeciesInfo = to.SpeciesString,
                                TrackedObjectId = to.TrackedObjectId,
                                Location = new Location { Latitude = d.Latitude, Longitude = d.Longitude, Altitude = d.Altitude, Time = d.Time },
                                Color = MapObject.Colors[colorIndex]
                            }
                        );
                    }
                    colorIndex++;
                    if (colorIndex == MapObject.Colors.Count) colorIndex = 0; 
                }
            }
            foreach(Route r in RoutesViewModel.Routes)
            {
                if (r.Selected)
                {
                    customMap.TracksCollection.Add(
                        new TrackObject() { MapData = r.Locations, Color = MapObject.Colors[colorIndex] }
                    );
                    colorIndex++;
                    if (colorIndex == MapObject.Colors.Count) colorIndex = 0;
                }
            }
        }

        private void MapTypeButton_Clicked(object sender, EventArgs e)
        {
            typePicker.Focus();
        }

        public void typePickerSelectedIndexChanged(object sender, EventArgs e)
        {
            if (typePicker.SelectedIndex == -1)
            {
                customMap.MapType = MapType.Street;
                typePicker.SelectedIndex = 0;
                mapTypeButton.Text = "Street";
            }
            else
            {
                string typeName = typePicker.Items[typePicker.SelectedIndex];
                switch (typeName)
                {
                    case "Street":
                        customMap.MapType = MapType.Street;
                        break;
                    case "Hybrid":
                        customMap.MapType = MapType.Hybrid;
                        break;
                    case "Satellite":
                        customMap.MapType = MapType.Satellite;
                        break;
                }
                mapTypeButton.Text = typeName;
            }
        }

        private void UpdatePins(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (ViewModel.MapObjects.Count > 0)
            {
                // Move map view to the last object position
                Position firstObjectPosition = new Position(ViewModel.MapObjects.Last().Location.Latitude, ViewModel.MapObjects.Last().Location.Longitude);
                customMap.MoveToRegion(MapSpan.FromCenterAndRadius(firstObjectPosition, Distance.FromMiles(0.5)));
            }
            
            customMap.UpdatePins(ViewModel.MapObjects);
        }

        void CrossGeolocator_Current_PositionError(object sender, PositionErrorEventArgs e)
        {

            labelGPSTrack.Text = "Location error: " + e.Error.ToString();
        }

        void CrossGeolocator_Current_PositionChanged(object sender, PositionEventArgs e)
        {

            Device.BeginInvokeOnMainThread(() =>
            {
                var position = e.Position;
                Positions.Add(new Location(position.Latitude, position.Longitude, position.Altitude,
                    position.Timestamp.UtcDateTime.Millisecond));
                updatesTracking++;
                
                // Keep only the last position map object
                // TODO nemať žiadne body na mojej trase?
                if (MyLastPosition != null)
                {
                    ViewModel.MapObjects.Remove(MyLastPosition);
                }
                MyLastPosition = new MapObject
                {
                    Location = new Location { Latitude = position.Latitude, Longitude = position.Longitude, Altitude = position.Altitude,
                        DateTime = position.Timestamp.DateTime.ToString(Constants.DateTimeFormat) + " UTC" },
                    Color = MapObject.Colors[4] // Red
                };
                ViewModel.MapObjects.Add(MyLastPosition);

                /*
                labelGPSTrack.Text = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
                    position.Timestamp, position.Latitude, position.Longitude,
                    position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);
                */
                Console.WriteLine("Position Status: {0}", position.Timestamp);
                Console.WriteLine("Position Latitude: {0}", position.Latitude);
                Console.WriteLine("Position Longitude: {0}", position.Longitude);

                customMap.RouteCoordinates.Add(new Position(position.Latitude, position.Longitude));
            });

        }

        private ICommand _showDetailCommand;

        public ICommand ShowDetailCommand
        {
            get { return _showDetailCommand = _showDetailCommand ?? new Command(async m => await ShowDetailAsync((ExtendedPin)m)); }
        }

        private async System.Threading.Tasks.Task ShowDetailAsync(ExtendedPin selectedPin)
        {
            TrackedObject to = await App.DB.GetTrackedObjectAsync(selectedPin.TrackedObjectId);
            await Navigation.PushAsync(new TODetailPage(new TODetailViewModel(to)));
        }
        
    }
}