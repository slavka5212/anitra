﻿using AnitraX.Models;
using AnitraX.ViewModels;
using Plugin.InputKit.Shared.Controls;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SelectTOsPage : ContentPage
	{
        TrackedObjectsViewModel viewModel;
        public SelectTOsPage (TrackedObjectsViewModel viewModel)
		{
            BindingContext = this.viewModel = viewModel;

            InitializeComponent ();
		}

        public SelectTOsPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.TrackedObjects.Count == 0)
                viewModel.LoadTrackedObjectsCommand.Execute(null);
            
        }

        private void SelectTOCommand(string toId)
        {
            foreach (TrackedObject item in viewModel.TrackedObjects)
            {
                if (item.TrackedObjectId == toId)
                    item.Selected = !item.Selected;
            }
        }

        private void SelectTOsBySpeciesCommand(object sender, EventArgs e)
        {

            if (sender is CheckBox cb)
            {
                foreach (Grouping<string,TrackedObject> group in viewModel.TrackedObjectsGrouped)
                {
                    if (cb.CommandParameter.ToString() == group.Key)
                    {
                        foreach(TrackedObject to in group.ToList())
                        {
                            to.Selected = cb.IsChecked;
                        }
                        group.Selected = cb.IsChecked;
                    }
                }
                InitializeComponent();
            }
        }
    }
}