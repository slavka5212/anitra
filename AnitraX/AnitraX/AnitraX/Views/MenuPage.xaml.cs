﻿using AnitraX.Models;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = new List<HomeMenuItem>
            {
                new HomeMenuItem {Id = MenuItemType.Map, Title="Map", IconSource = "ic_menu_map.xml" },
                new HomeMenuItem {Id = MenuItemType.TO, Title="Tracked Objects", IconSource = "ic_menu_tracked_object.xml" },
                new HomeMenuItem {Id = MenuItemType.MyTracks, Title="My tracks", IconSource = "ic_location_searching.xml" },
                new HomeMenuItem {Id = MenuItemType.Settings, Title="Settings", IconSource = "ic_menu_settings.xml" },
                new HomeMenuItem {Id = MenuItemType.Logout, Title = "Logout", IconSource = "ic_menu_lock.xml" },
            };

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };

            if(Application.Current.Properties.ContainsKey("last_data_synch"))
            {
                LastSynchLabel.Text = "Last data synchronization: " + Application.Current.Properties["last_data_synch"].ToString();
            }
            else
            {
                LastSynchLabel.Text = "No data synchronization";
            }
        }

        private async System.Threading.Tasks.Task SynchronizeButton_ClickedAsync(object sender, EventArgs e)
        {
            Application.Current.Properties["last_data_synch"] = DateTime.Now.ToLocalTime().ToString(Constants.DateTimeFormat);

            // update Tracked Objects data
            await App.DB.StoreTOAsync(App.restSharpcaller.GetTOList());
            // update Routes data
            App.restSharpcaller.GetRoutesList();
            // post Routes data
            App.restSharpcaller.SendRoutes();

            LastSynchLabel.Text = "Last data synchronization: " + Application.Current.Properties["last_data_synch"].ToString();
        }
    }
}