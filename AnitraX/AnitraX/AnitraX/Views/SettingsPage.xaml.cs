﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();

            minSecGPS.Text = Constants.MinSecGPSdataCollection.ToString();
            synchPicker.Items.Add("Every day");
            synchPicker.SelectedIndex = 0;
        }

        private void Synch_Switch_Toggled(object sender, ToggledEventArgs e)
        {
            // Switch is now e.Value
            if (e.Value)
            {
                synchPicker.IsEnabled = true;
                Application.Current.Properties["data_synchronization"] = "day";
            } else
            {
                synchPicker.IsEnabled = false;
                Application.Current.Properties["data_synchronization"] = null;
            }
        }
        
        private void Editor_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.NewTextValue))
            {
                ((Entry)sender).Text = 0.ToString();
                return;
            }

            double _;
            if (!double.TryParse(e.NewTextValue, out _))
                ((Entry)sender).Text = e.OldTextValue;
            else
                ((Entry)sender).Text = Convert.ToInt32(e.NewTextValue) < Constants.MinSecGPSdataCollection ?
                    Constants.MinSecGPSdataCollection.ToString() : Convert.ToInt32(e.NewTextValue).ToString();

            Application.Current.Properties["gps_data_sec"] = ((Entry)sender).Text;
        }
    }
}