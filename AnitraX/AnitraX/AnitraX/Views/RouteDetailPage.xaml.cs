﻿using AnitraX.MapCS;
using AnitraX.Models;
using AnitraX.ViewModels;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RouteDetailPage : ContentPage
	{
        RouteDetailViewModel viewModel;
        public RouteDetailPage(RouteDetailViewModel viewModel)
        {
            InitializeComponent();
            // set back arrow to ""
            NavigationPage.SetBackButtonTitle(this, "");

            BindingContext = this.viewModel = viewModel;
        }

        public RouteDetailPage()
        {
            InitializeComponent();

            BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            AddRoute();
        }

        async void Edit_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RouteEditPage(viewModel));
        }

        private void AddRoute()
        {
            var to = this.viewModel.RouteObject;
            if (to.Locations == null || to.Locations.Count == 0) return;

            Location location = to.Locations.First<Location>();
            
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(
               new Position(location.Latitude, location.Longitude),
               Distance.FromMiles(1.0)));
            
            foreach(Location l in viewModel.RouteObject.Locations)
            {
                customMap.RouteCoordinates.Add(l.getPosition());

                var mo = new MapObject
                {
                    Location = new Location { Latitude = l.Latitude, Longitude = l.Longitude, Altitude = l.Altitude, Time = l.Time },
                    Color = MapObject.Colors[4] // Red color
                };
                customMap.Items.Add(mo.AsPin());
            }
        }
    }
}