﻿using AnitraX.Models;
using AnitraX.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TracksPage : ContentPage
	{
        TracksViewModel viewModel;

        public TracksPage ()
		{
			InitializeComponent ();

            BindingContext = viewModel = new TracksViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            
            if (viewModel.Routes.Count == 0)
                viewModel.LoadRoutesCommand.Execute(RoutesListView);

            MessagingCenter.Subscribe<string>(this, "AddRoute", (arg) =>
            {
                // refresh
                InitializeComponent();
            });
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Route;
            if (item == null)
                return;

            await Navigation.PushAsync(new RouteDetailPage(new RouteDetailViewModel(item)));

            // Manually deselect item.
            RoutesListView.SelectedItem = null;
        }
    }
}