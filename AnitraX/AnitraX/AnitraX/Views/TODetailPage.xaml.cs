﻿using AnitraX.MapCS;
using AnitraX.Models;
using AnitraX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TODetailPage : ContentPage
	{
        TODetailViewModel viewModel;
        public TODetailPage (TODetailViewModel viewModel)
		{
			InitializeComponent ();

            BindingContext = this.viewModel = viewModel;
            
        }

        public TODetailPage()
        {
            InitializeComponent();
            
            BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            AddPin();
        }

        private void AddPin()
        {
            var to = this.viewModel.TO;
            var mo = new MapObject
            {
                Name = to.TrackedObjectCodeAndName,
                DeviceInfo = to.DeviceCodeString,
                SpeciesInfo = to.SpeciesString,
                Location = new Location { Latitude = to.LastPositionLatitude, Longitude = to.LastPositionLongitude, DateTime = to.LastPositionTime},
                Color = MapObject.Colors[4] // Red color
            };
            customMap.Items.Add(mo.AsPin());
            customMap.SelectedPin = mo.AsPin();
            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(
               new Position(to.LastPositionLatitude, to.LastPositionLongitude),
               Distance.FromMiles(1.0)));
        }
    }
}