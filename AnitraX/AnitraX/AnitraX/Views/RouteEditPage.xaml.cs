﻿using AnitraX.MapCS;
using AnitraX.Models;
using AnitraX.ViewModels;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace AnitraX.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RouteEditPage : ContentPage
	{
        RouteDetailViewModel viewModel;
        public RouteEditPage(RouteDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }
        
        public RouteEditPage ()
		{
			InitializeComponent ();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();

            AddRoute();
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            var to = this.viewModel.RouteObject;
            await App.DB.StoreRouteAsync(to);

            await Navigation.PopToRootAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        private void AddRoute()
        {
            var to = this.viewModel.RouteObject;
            if (to.Locations == null || to.Locations.Count == 0) return;

            Location location = to.Locations.First<Location>();

            customMap.MoveToRegion(MapSpan.FromCenterAndRadius(
               new Position(location.Latitude, location.Longitude),
               Distance.FromMiles(1.0)));

            foreach (Location l in viewModel.RouteObject.Locations)
            {
                customMap.RouteCoordinates.Add(l.getPosition());

                var mo = new MapObject
                {
                    Location = new Location { Latitude = l.Latitude, Longitude = l.Longitude, Altitude = l.Altitude, Time = l.Time },
                    Color = MapObject.Colors[4] // Red color
                };
                customMap.Items.Add(mo.AsPin());
            }
        }
    }
}