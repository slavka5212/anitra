﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace AnitraX.MapCS
{
    public interface ILocation
    {
        double Latitude { get; set; }
        double Longitude { get; set; }
        Position getPosition();
    }
}
