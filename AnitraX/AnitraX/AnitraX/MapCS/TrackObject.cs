﻿using AnitraX.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.MapCS
{
    /// <summary>
    /// TrackObject is composed of MapData - points/pins on the map and the Color used to color them on the map.
    /// </summary>
    public class TrackObject
    {
        public List<Location> MapData { get; set; }

        public string Color { get; set; }

        public TrackObject()
        {

        }
    }
}
