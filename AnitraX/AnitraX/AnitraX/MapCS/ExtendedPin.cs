﻿using AnitraX.Models;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms.Maps;

namespace AnitraX.MapCS
{
    public class ExtendedPin : Pin
    {
        private object _markerId;
        private object _circleId;
        private object _id;

        public string Name { get; set; }
        public string DeviceInfo { get; set; }
        public string SpeciesInfo { get; set; }
        public string TrackedObjectId { get; set; }
        public Location Location { get; set; }
        public string PinColor { get; set; }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public object MarkerId
        {
            get => _markerId;
            set
            {
                _markerId = value;
                _id = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public object CircleId
        {
            get => _circleId;
            set
            {
                _circleId = value;
            }
        }
    }
}
 