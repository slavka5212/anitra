﻿using AnitraX.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms.Maps;

namespace AnitraX.MapCS
{
    public static class MapExtensions
    {
        public static ExtendedPin AsPin(this IMapModel item)
        {
            var location = item.Location;
            var position = location != null ? location.getPosition() : Location.getDefaultPosition();
            var color = item.Color;
            return new ExtendedPin {
                Name = item.Name,
                DeviceInfo = item.DeviceInfo,
                SpeciesInfo = item.SpeciesInfo,
                TrackedObjectId = item.TrackedObjectId,
                Position = position,
                Location = location,
                PinColor = color
            };
        }
    }
}
