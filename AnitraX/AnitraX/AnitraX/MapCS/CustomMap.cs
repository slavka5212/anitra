﻿using AnitraX.Models;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace AnitraX.MapCS
{
    public class CustomMap : Xamarin.Forms.Maps.Map
    {
        private readonly ObservableCollection<ExtendedPin> _items = new ObservableCollection<ExtendedPin>();
        private readonly ObservableCollection<Position> _routes = new ObservableCollection<Position>();
        private readonly ObservableCollection<TrackObject> _tracks = new ObservableCollection<TrackObject>();

        public static readonly BindableProperty SelectedPinProperty =
            BindableProperty.Create( nameof(SelectedPin), typeof(ExtendedPin), typeof(CustomMap), null);

        public ExtendedPin SelectedPin
        {
            get { return (ExtendedPin)base.GetValue(SelectedPinProperty); }
            set { base.SetValue(SelectedPinProperty, value); }
        }

        public ObservableCollection<Position> RouteCoordinates
        {
            get { return _routes; }
        }

        public ObservableCollection<TrackObject> TracksCollection
        {
            get { return _tracks; }
        }

        public ICommand ShowDetailCommand { get; set; }

        public ObservableCollection<ExtendedPin> Items
        {
            get { return _items; }
        }

        public void UpdatePins(IEnumerable<IMapModel> items)
        {
            Items.Clear();
            foreach (var item in items)
            {
                Items.Add(item.AsPin());
            }
        }

        public bool IsLocationAvailable()
        {
            if (!CrossGeolocator.IsSupported)
                return false;

            return CrossGeolocator.Current.IsGeolocationAvailable;
        }
        
    }
}
