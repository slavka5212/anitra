﻿using AnitraX.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.MapCS
{
    public interface IMapModel
    {
        string Name { get; set; }
        string Details { get; set; }
        string DeviceInfo { get; set; }
        string SpeciesInfo { get; set; }
        string TrackedObjectId { get; set; }
        Location Location { get; set; }
        string ImageUrl { get; set; }
        string Color { get; set; }
    }
}
