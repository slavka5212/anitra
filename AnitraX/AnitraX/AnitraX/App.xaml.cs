﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AnitraX.Views;
using AnitraX.Data;
using AnitraX.Models;
using System.Collections.Generic;
using AnitraX.Resources;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace AnitraX
{
    public partial class App : Application
    {
        public static bool IsUserLoggedIn { get; set; }
        public static double ScreenHeight;
        public static double ScreenWidth;
        public static RestSharpCaller restSharpcaller;

        public static TrackedObjectManager TOManager { get; private set; }

        public static string DatabasePath = string.Empty;

        public static Database DB { get; set; }

        /// <summary>
        /// The main application method. 
        /// Not used. Used the one with databasePath.
        /// </summary>
        public App()
        {
            InitializeComponent();
            
            IsUserLoggedIn = Current.Properties.ContainsKey("is_logged_in") ? Convert.ToBoolean(Current.Properties["is_logged_in"]) : false;
            if (!IsUserLoggedIn)
            {
                MainPage = new LoginPage();
            }
            else
            {
                MainPage = new MainPage();
            }
        }

        /// <summary>
        /// The main application method with the database path.
        /// </summary>
        /// <param name="databasePath"></param>
        public App(string databasePath)
        {
            InitializeComponent();

            DatabasePath = databasePath;
            // fill SQLite database with data
            DB = new Database(databasePath);
            SQLiteTestData.run(); // run to update DB
            

            restSharpcaller = new RestSharpCaller("https://anitra.cz/demo/api/");
            
            TOManager = new TrackedObjectManager();

            // TOManager.TOlist = restSharpcaller.GetTOList();
            TOManager.TOlist = new List<TrackedObject> { };
            /*
            Console.WriteLine("Found {0} products: ", TOManager.TOlist.Count);
            foreach (var product in TOManager.TOlist)
            {
                Console.WriteLine("\t{0} (ID:{1}) locality {2:c}", product.TrackedObjectName
                    , product.TrackedObjectId, product.TrackingStartLocality);
            }*/

            /* skip login for development 
            IsUserLoggedIn = Current.Properties.ContainsKey("is_logged_in") ? Convert.ToBoolean(Current.Properties["is_logged_in"]) : false;
            if (!IsUserLoggedIn)
            {
                MainPage = new LoginPage();
            }
            else
            {
                MainPage = new MainPage();
            }  skip login for development */
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
