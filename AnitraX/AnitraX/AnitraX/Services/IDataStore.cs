﻿using AnitraX.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnitraX.Services
{
    /// <summary>
    /// Interface used for storing and working with internal/testing data.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataStore<T>
    {
        Task<bool> AddItemAsync(T item);
        Task<bool> UpdateItemAsync(T item);
        Task<T> GetTOAsync(string id);
        Task<IEnumerable<T>> GetTOsAsync(bool forceRefresh = false);
    }
}
