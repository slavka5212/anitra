﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnitraX.Models;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace AnitraX.Services
{
    /// <summary>
    /// MockDataStore service provides data: TrackObject list.
    /// These data are for testing.
    /// </summary>
    public class MockDataStore : IDataStore<TrackedObject>
    {
        List<TrackedObject> TOs;

        public MockDataStore()
        {
            TOs = new List<TrackedObject>();
            var mockTOs = new List<TrackedObject>
            {
                new TrackedObject {
                    TrackedObjectId = Guid.NewGuid().ToString(),
                    TrackedObjectName = "First",
                    TrackedObjectCode = "123",
                    DeviceCode = "B1TEST",
                    TrackedFrom = "18.12.2018",
                    LastPositionLatitude = 48.970139,
                    LastPositionLongitude = 17.188459,
                    MapData = new List<Location> {
                        new Location {Latitude = 50.053406,Longitude = 15.914619,Time = 1518248134},
                        new Location {Latitude = 50.056094,Longitude = 15.947017,Time = 1518255358},
                        new Location {Latitude = 50.05144,Longitude = 15.929835,Time = 1518262572},
                        new Location {Latitude = 50.045208,Longitude = 15.905037,Time = 1518269726},
                        new Location {Latitude = 50.067898,Longitude = 15.88212,Time = 1518276957},
                        new Location {Latitude = 50.067574,Longitude = 15.882104,Time = 1518284142},
                        new Location {Latitude = 50.067638,Longitude = 15.88186,Time = 1518291357},
                        new Location {Latitude = 50.067841,Longitude = 15.882459,Time = 1518298543},
                        new Location {Latitude = 50.068053,Longitude = 15.881857,Time = 1518323323},
                        new Location {Latitude = 50.055759,Longitude = 15.903136,Time = 1518326906},
                        new Location {Latitude = 50.044513,Longitude = 15.905447,Time = 1518330501},            
                    }
                },
                new TrackedObject {
                    TrackedObjectId = Guid.NewGuid().ToString(),
                    TrackedObjectName = "Obristvi_02",
                    TrackedObjectCode = "18014",
                    DeviceCode = "18014",
                    TrackedFrom = "16.06.2018",
                    LastPositionLatitude = 48.970139,
                    LastPositionLongitude = 17.188459 },
                new TrackedObject {
                    TrackedObjectId = Guid.NewGuid().ToString(),
                    TrackedObjectName = "HUSLIK_02_Falper",
                    TrackedObjectCode = "18017",
                    DeviceCode = "ANITRA 017",
                    TrackedFrom = "29.06.2018",
                    LastPositionLatitude = 48.970139,
                    LastPositionLongitude = 17.188459 },
                new TrackedObject {
                    TrackedObjectId = Guid.NewGuid().ToString(),
                    TrackedObjectName = "Hostin_02",
                    TrackedObjectCode = "18020",
                    DeviceCode = "18020",
                    TrackedFrom = "23.06.2018",
                    LastPositionLatitude = 48.970139,
                    LastPositionLongitude = 17.188459 },
                new TrackedObject {
                    TrackedObjectId = Guid.NewGuid().ToString(),
                    TrackedObjectName = "HUSLIK_01_Bubbub",
                    TrackedObjectCode = "18026_1",
                    DeviceCode = "ANITRA 026",
                    TrackedFrom = "29.06.2018",
                    LastPositionLatitude = 48.970139,
                    LastPositionLongitude = 17.188459 },
                new TrackedObject {
                    TrackedObjectId = Guid.NewGuid().ToString(),
                    TrackedObjectName = "HUSLIK_03_Bubbub",
                    TrackedObjectCode = "18026_2",
                    DeviceCode = "ANITRA 026",
                    TrackedFrom = "31.12.2018",
                    LastPositionLatitude = 48.970139,
                    LastPositionLongitude = 17.188459 },
            };

            foreach (var TO in mockTOs)
            {
                TOs.Add(TO);
            }
        }

        public async Task<bool> AddItemAsync(TrackedObject item)
        {
            TOs.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(TrackedObject item)
        {
            var oldItem = TOs.Where((TrackedObject arg) => arg.TrackedObjectId == item.TrackedObjectId).FirstOrDefault();
            TOs.Remove(oldItem);
            TOs.Add(item);

            return await Task.FromResult(true);
        }
        
        public async Task<TrackedObject> GetTOAsync(string id)
        {
            return await Task.FromResult(TOs.FirstOrDefault(s => s.TrackedObjectId == id));
        }

        public async Task<IEnumerable<TrackedObject>> GetTOsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(TOs);
        }
    }
}