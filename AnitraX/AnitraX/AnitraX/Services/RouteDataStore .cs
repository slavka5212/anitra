﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnitraX.Models;

namespace AnitraX.Services
{
    /// <summary>
    /// RouteDataStore service provides data: Routes list.
    /// These data are for testing.
    /// </summary>
    public class RouteDataStore : IDataStore<Route>
    {
        List<Route> Routes;

        public RouteDataStore()
        {
            Routes = new List<Route>();
            var mockTOs = new List<Route>
            {
                 new Route {
                     Locations = new List<Location> {
                        new Location {Latitude = 50.053406,Longitude = 15.914619,Time = 1518248134},
                        new Location {Latitude = 50.056094,Longitude = 15.947017,Time = 1518255358},
                        new Location {Latitude = 50.05144,Longitude = 15.929835,Time = 1518262572},
                        new Location {Latitude = 50.045208,Longitude = 15.905037,Time = 1518269726},
                        new Location {Latitude = 50.067898,Longitude = 15.88212,Time = 1518276957},
                        new Location {Latitude = 50.067574,Longitude = 15.882104,Time = 1518284142},
                        new Location {Latitude = 50.067638,Longitude = 15.88186,Time = 1518291357},
                        new Location {Latitude = 50.067841,Longitude = 15.882459,Time = 1518298543},
                        new Location {Latitude = 50.068053,Longitude = 15.881857,Time = 1518323323},
                        new Location{Latitude = 50.055759,Longitude = 15.903136,Time = 1518326906},
                        new Location {Latitude = 50.044513,Longitude = 15.905447,Time = 1518330501},
                     },
                     Note = "Note tracking"
                 },
                 new Route {
                     Locations = new List<Location> {
                        new Location {Latitude = 50.053406,Longitude = 15.914619,Time = 1518248134},
                        new Location {Latitude = 50.056094,Longitude = 15.947017,Time = 1518255358},
                        new Location {Latitude = 50.05144,Longitude = 15.929835,Time = 1518262572},
                        new Location {Latitude = 50.045208,Longitude = 15.905037,Time = 1518269726},
                        new Location {Latitude = 50.067898,Longitude = 15.88212,Time = 1518276957},
                        new Location {Latitude = 50.067574,Longitude = 15.882104,Time = 1518284142},
                        new Location {Latitude = 50.067638,Longitude = 15.88186,Time = 1518291357},
                        new Location {Latitude = 50.067841,Longitude = 15.882459,Time = 1518298543},
                        new Location {Latitude = 50.068053,Longitude = 15.881857,Time = 1518323323},
                        new Location{Latitude = 50.055759,Longitude = 15.903136,Time = 1518326906},
                        new Location {Latitude = 50.044513,Longitude = 15.905447,Time = 1518330501},
                     },
                     Note = "Second tracking"
                 },
            };

            foreach (var TO in mockTOs)
            {
                Routes.Add(TO);
            }
        }

        public async Task<bool> AddItemAsync(Route item)
        {
            Routes.Add(item);

            return await Task.FromResult(true);
        }

        public Task<Route> GetTOAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Route>> GetTOsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(Routes);
        }

        public Task<bool> UpdateItemAsync(Route item)
        {
            throw new NotImplementedException();
        }
        
    }
}