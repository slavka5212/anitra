﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Models
{
    public enum MenuItemType
    {
        Map,
        TO,
        MyTracks,
        Settings,
        Logout
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }

        public string IconSource { get; set; }
    }
}
