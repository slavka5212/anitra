﻿using AnitraX.MapCS;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Maps;

namespace AnitraX.Models
{
    /// <summary>
    /// Location represents a data given from API or GPS.
    /// A location can be identified by TrackedObjectId or RouteId.
    /// </summary>
    public class Location : ILocation
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [ForeignKey(typeof(TrackedObject))]
        public string TrackedObjectId { get; set; }
        [ForeignKey(typeof(Route))]
        public int RouteId { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public long Time { get; set; }
        public string DateTime { get; set; }

        public Location(double latitude, double longitude, string dateTime)
        {
            Latitude = latitude;
            Longitude = longitude;
            DateTime = dateTime;
        }

        public Location(double latitude, double longitude, double altitude, long time)
        {
            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
            Time = time;
        }
        public Location(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
        public Location()
        {
        }
        public Position getPosition()
        {
            return new Position(Latitude, Longitude);
        }
        public static Position getDefaultPosition()
        {
            return new Position(0, 0);
        }

        public string getDateTimeString()
        {
            if (DateTime != null)
            {
                return DateTime;
            }
            else if (Time != 0)
            {
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(Time).DateTime;
                return dateTime.ToString(Constants.DateTimeFormat) + " UTC";
            }
            return null;
        }

        public string getAltitudeString()
        {
            return "Altitude: " + Altitude + "m";
        }
    }
}
