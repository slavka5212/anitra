﻿using Newtonsoft.Json;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Models
{
    /// <summary>
    /// Route represents tracked user device locations.
    /// </summary>
    public class Route
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; } /* Internal Id */
        [JsonProperty(PropertyName = "RouteId")]
        public int RouteId { get; set; } /* External Id - REST */
        [OneToMany]
        [JsonProperty(PropertyName = "MapData")]
        public List<Location> Locations { get; set; }
        public DateTime DateTime { get; set; } /* Creation date */
        [JsonProperty(PropertyName = "Time")]
        public long Time { get; set; }
        [JsonProperty(PropertyName = "Name")]
        public string Name { get; set;}
        [JsonProperty(PropertyName = "Note")]
        public string Note { get; set; }
        [JsonProperty(PropertyName = "UserId")]
        public int UserId { get; set; }
        public bool Selected { get; set; }
        [JsonProperty(PropertyName = "IsDeleted")]
        public bool IsDeleted { get; set; }

        public string TimeString
        {
            get
            {
                if (Time == 0)
                {
                    return "";
                }
                DateTime dateTime = DateTimeOffset.FromUnixTimeSeconds(Time).DateTime;
                return dateTime.ToString(Constants.DateTimeFormat) + " UTC";
            }
        }
    }
}
