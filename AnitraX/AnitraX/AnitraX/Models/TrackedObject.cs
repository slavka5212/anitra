﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Models
{
    /// <summary>
    /// Tracked Object represents data model from the REST API
    /// Each variable has the name given by GET response(tracked-object/list)
    /// </summary>
    public class TrackedObject
    {
        [PrimaryKey]
        public string TrackedObjectId { get; set; }
        public string TrackedObjectCode { get; set; }
        public string TrackedObjectName { get; set; }
        public string TrackedObjectNote { get; set; }
        public string TrackedObjectColour { get; set; }
        public string DeviceId { get; set; }
        public string DeviceCode { get; set; }
        public string DeviceGSM { get; set; }
        public string DeviceStatusName { get; set; }
        public string LastPositionCountry { get; set; }
        public string LastPositionAdmin1 { get; set; }
        public string LastPositionAdmin2 { get; set; }
        public string LastPositionSettlement { get; set; }
        public double LastPositionLatitude { get; set; }
        public double LastPositionLongitude { get; set; }
        public string LastPositionTime { get; set; }
        public string TrackingStartLocality { get; set; }
        public string TrackedFrom { get; set; }
        public string SpeciesID { get; set; }
        public string SpeciesName_Scientific { get; set; }
        public string SpeciesName_English { get; set; }
        public string IndividualSex { get; set; }
        public string CurrentAge { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.CascadeRead)]
        public List<Location> MapData { get; set; }
        public bool Selected { get; set; }

        public class GetDateTime
        {
            public string date { get; set; }
            public int timezone_type { get; set; }
            public string timezone { get; set; }
        }

        public string TrackedObjectCodeAndName
        {

            get
            {
                return string.Format("{0} ({1})", TrackedObjectCode, TrackedObjectName);
            }
        }

        public string DeviceCodeString
        {

            get
            {
                return string.Format("Device: {0}", DeviceCode);
            }
        }

        public string SpeciesString
        {

            get
            {
                if (SpeciesName_Scientific != null)
                    return string.Format("{0} ({1} {2})", SpeciesName_Scientific, IndividualSex, CurrentAge);
                else return "";
            }
        }

        public string LastData
        {

            get
            {
                return string.Format("{0}  {1}, {2}, {3}, {4}", LastPositionCountry, LastPositionAdmin1, LastPositionAdmin2, LastPositionSettlement, LastPositionTime);
            }
        }
    }
}
