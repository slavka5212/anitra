﻿using AnitraX.MapCS;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Models
{
    /// <summary>
    /// MapObject represents an object on the map.
    /// This object is a tracked object in given time and given location.
    /// </summary>
    public class MapObject : IMapModel
    {
        public static readonly List<string> Colors = new List<string> {
            "#FFB300",    // Vivid Yellow
            "#803E75",    // Strong Purple
            "#FF6800",    // Vivid Orange
            "#A6BDD7",    // Very Light Blue
            "#C10020",    // Vivid Red
            "#CEA262",    // Grayish Yellow
            "#817066",    // Medium Gray
            "#007D34",    // Vivid Green
            "#F6768E",    // Strong Purplish Pink
            "#00538A",    // Strong Blue
            "#FF7A5C",    // Strong Yellowish Pink
            "#53377A",    // Strong Violet
            "#FF8E00",    // Vivid Orange Yellow
            "#B32851",    // Strong Purplish Red
            "#F4C800",    // Vivid Greenish Yellow
            "#7F180D",    // Strong Reddish Brown
            "#93AA00",    // Vivid Yellowish Green
            "#593315",    // Deep Yellowish Brown
            "#F13A13",    // Vivid Reddish Orange
            "#232C16",    // Dark Olive Green
        };

        public string Name { get; set; }
        public string Details { get; set; }
        public string DeviceInfo { get; set; }
        public string SpeciesInfo { get; set; }
        public string TrackedObjectId { get; set; }
        public Location Location { get; set; }
        public string ImageUrl { get; set; }
        public string Color { get; set; }
    }
}
