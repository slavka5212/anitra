﻿using AnitraX.MapCS;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AnitraX.ViewModels
{
    public class MapViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<IMapModel> _objects = new ObservableCollection<IMapModel>();
        public ObservableCollection<IMapModel> MapObjects
        {
            get { return _objects; }
            set
            {
                _objects = value;

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("MapObjects"));
                }
            }
        }
    }
}