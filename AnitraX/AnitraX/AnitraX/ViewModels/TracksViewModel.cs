﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;
using AnitraX.Models;
using System.Collections.Generic;

namespace AnitraX.ViewModels
{
    public class TracksViewModel : BaseViewModel
    {
        public ObservableCollection<Route> Routes { get; set; }
        public Command LoadRoutesCommand { get; set; }
        public TracksViewModel()
        {
            Title = "My Tracks";
            Routes = new ObservableCollection<Route>();

            LoadRoutesCommand = new Command(async () => await ExecuteLoadRoutesCommand());
        }

        async Task ExecuteLoadRoutesCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Routes.Clear();

                List<Route> routes = await App.DB.GetRoutesAsync(); 

                foreach (Route item in routes)
                {
                    Routes.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
