﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using AnitraX.Models;
using System.Linq;

namespace AnitraX.ViewModels
{
    public class TrackedObjectsViewModel : BaseViewModel
    {
        public ObservableCollection<TrackedObject> TrackedObjects { get; set; }
        public ObservableCollection<Grouping<string, TrackedObject>> TrackedObjectsGrouped { get; set; }
        public Command LoadTrackedObjectsCommand { get; set; }

        public TrackedObjectsViewModel()
        {
            Title = "Tracked Objects";
            TrackedObjects = new ObservableCollection<TrackedObject>(App.TOManager.TOlist);

            LoadTrackedObjectsCommand = new Command(async () => await ExecuteLoadTOsCommand());
        }

        async Task ExecuteLoadTOsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;
            
            try
            {
                TrackedObjects.Clear();
               
                var TOs = await App.DB.GetTrackedObjectsAsync();
                foreach (TrackedObject item in TOs)
                {
                    TrackedObjects.Add(item);
                }

                // create sorted ObservableCollection with TrackedObjects
                var sorted = from to in TrackedObjects
                             orderby to.SpeciesName_Scientific
                             group to by to.SpeciesName_Scientific into toGroup
                             select new Grouping<string, TrackedObject>(toGroup.Key, toGroup);
                TrackedObjectsGrouped = new ObservableCollection<Grouping<string, TrackedObject>>(sorted);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }    
}