﻿using AnitraX.Models;

namespace AnitraX.ViewModels
{
    public class RouteDetailViewModel : BaseViewModel
    {
        public Route RouteObject { get; set; }
        public RouteDetailViewModel(Route route = null)
        {
            Title = route?.Name;
            RouteObject = route;
        }
    }
}
