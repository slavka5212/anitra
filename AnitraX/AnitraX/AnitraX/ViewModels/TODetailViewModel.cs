﻿using AnitraX.Models;

namespace AnitraX.ViewModels
{
    public class TODetailViewModel : BaseViewModel
    {
        public TrackedObject TO { get; set; }
        public TODetailViewModel(TrackedObject to = null)
        {
            Title = to?.TrackedObjectName;
            TO = to;
        }
    }
}
