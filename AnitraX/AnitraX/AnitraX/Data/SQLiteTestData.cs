﻿using AnitraX.Models;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Data
{
    public class SQLiteTestData
    {
        public static void run()
        {
            SQLiteConnection conn = new SQLiteConnection(App.DatabasePath);
            conn.CreateTable<Location>();
            conn.CreateTable<Route>();
            conn.CreateTable<TrackedObject>();
            conn.DeleteAll<Location>();
            conn.DeleteAll<Route>();
            conn.DeleteAll<TrackedObject>();

            // TOs
            foreach (TrackedObject to in mockTOs)
            {
                if (to.MapData != null)
                {
                    foreach(Location l in to.MapData)
                    {
                        l.TrackedObjectId = to.TrackedObjectId;
                        conn.Insert(l);
                    }
                    //conn.InsertAll(to.MapData);
                }
                conn.InsertWithChildren(to, recursive: true);
            }

            // Routes
            foreach (Route r in mockRoutes)
            {
                if (r.Locations != null)
                {
                    foreach (Location l in r.Locations)
                    {
                        l.RouteId = r.Id;

                    }
                    conn.InsertAll(r.Locations);
                }
                conn.InsertWithChildren(r, recursive: true);
            }
        }
        
        private static List<TrackedObject> mockTOs = new List<TrackedObject>
        {
            new TrackedObject {
                TrackedObjectId = "1",
                TrackedObjectName = "First",
                TrackedObjectCode = "123",
                DeviceCode = "B1TEST",
                TrackedFrom = "18.12.2018",
                // Added for testing 
                DeviceStatusName = "Archive",
                SpeciesName_Scientific = "Milvus milvus",
                IndividualSex = "Male",
                CurrentAge = "1K",
                // end added for testing 
                LastPositionLatitude = 48.970139,
                LastPositionLongitude = 17.188459,
                MapData = new List<Location> {
                    new Location {Latitude = 50.053406,Longitude = 15.914619,Time = 1518248134},
                    new Location {Latitude = 50.056094,Longitude = 15.947017,Time = 1518255358},
                    new Location {Latitude = 50.05144,Longitude = 15.929835,Time = 1518262572},
                    new Location {Latitude = 50.045208,Longitude = 15.905037,Time = 1518269726},
                    new Location {Latitude = 50.067898,Longitude = 15.88212,Time = 1518276957},
                    new Location {Latitude = 50.067574,Longitude = 15.882104,Time = 1518284142},
                    new Location {Latitude = 50.067638,Longitude = 15.88186,Time = 1518291357},
                    new Location {Latitude = 50.067841,Longitude = 15.882459,Time = 1518298543},
                    new Location {Latitude = 50.068053,Longitude = 15.881857,Time = 1518323323},
                    new Location {Latitude = 50.055759,Longitude = 15.903136,Time = 1518326906},
                    new Location {Latitude = 50.044513,Longitude = 15.905447,Time = 1518330501},
                }
            },
            new TrackedObject {
                TrackedObjectId = "2",
                TrackedObjectName = "Obristvi_02",
                TrackedObjectCode = "18014",
                DeviceCode = "18014",
                TrackedFrom = "16.06.2018",
                DeviceStatusName = "Archive",
                SpeciesName_Scientific = "Milvus milvus",
                IndividualSex = "Male?",
                CurrentAge = "1K",
                LastPositionCountry = "CZ",
                LastPositionAdmin1 = "Obřiství",
                LastPositionAdmin2 = "Mělník District",
                LastPositionSettlement = "Central Bohemian Region",
                LastPositionTime = "19.07.2018 17:33Z",
                LastPositionLatitude = 48.970139,
                LastPositionLongitude = 17.188459,
                MapData = new List<Location> {
                    new Location {Latitude = 51.053406,Longitude = 15.914619,Time = 1518248134},
                    new Location {Latitude = 51.056094,Longitude = 15.947017,Time = 1518255358},
                    new Location {Latitude = 51.05144,Longitude = 15.929835,Time = 1518262572},
                    new Location {Latitude = 51.045208,Longitude = 15.905037,Time = 1518269726},
                    new Location {Latitude = 51.067898,Longitude = 15.88212,Time = 1518276957},
                    new Location {Latitude = 51.067574,Longitude = 15.882104,Time = 1518284142},
                    new Location {Latitude = 51.067638,Longitude = 15.88186,Time = 1518291357},
                    new Location {Latitude = 51.067841,Longitude = 15.882459,Time = 1518298543},
                    new Location {Latitude = 51.068053,Longitude = 15.881857,Time = 1518323323},
                    new Location {Latitude = 51.055759,Longitude = 15.903136,Time = 1518326906},
                    new Location {Latitude = 51.044513,Longitude = 15.905447,Time = 1518330501},
                }
            },
            new TrackedObject {
                TrackedObjectId = "3",
                TrackedObjectName = "HUSLIK_02_Falper",
                TrackedObjectCode = "18017",
                DeviceCode = "ANITRA 017",
                TrackedFrom = "29.06.2018",
                DeviceStatusName = "Archive",
                SpeciesName_Scientific = "Falco peregrinus",
                IndividualSex = "Female",
                CurrentAge = "+3K",
                LastPositionCountry = "PL",
                LastPositionAdmin1 = "Gmina Sulejów",
                LastPositionAdmin2 = "piotrkowski",
                LastPositionSettlement = "Łódź Voivodeship",
                LastPositionTime = "31.07.2018 18:58Z",
                LastPositionLatitude = 48.970139,
                LastPositionLongitude = 17.188459 },
            new TrackedObject {
                TrackedObjectId = "4",
                TrackedObjectName = "Hostin_02",
                TrackedObjectCode = "18020",
                DeviceCode = "18020",
                TrackedFrom = "23.06.2018",
                DeviceStatusName = "Archive",
                SpeciesName_Scientific = "Milvus milvus",
                IndividualSex = "Female?",
                CurrentAge = "1K",
                LastPositionCountry = "CZ",
                LastPositionAdmin1 = "Hostín",
                LastPositionAdmin2 = "Mělník District",
                LastPositionSettlement = "Central Bohemian Region",
                LastPositionTime = "23.06.2018 14:11Z",
                LastPositionLatitude = 48.970139,
                LastPositionLongitude = 17.188459 },
            new TrackedObject {
                TrackedObjectId = "5",
                TrackedObjectName = "HUSLIK_01_Bubbub",
                TrackedObjectCode = "18026_1",
                DeviceCode = "ANITRA 026",
                TrackedFrom = "29.06.2018",
                SpeciesName_Scientific = "Bubo bubo",
                IndividualSex = "Female",
                CurrentAge = "+3K",
                LastPositionCountry = "CZ",
                LastPositionAdmin1 = "Městec Králové",
                LastPositionAdmin2 = "Nymburk",
                LastPositionSettlement = "Středočeský kraj",
                LastPositionTime = "12.07.2018 05:33Z",
                LastPositionLatitude = 48.970139,
                LastPositionLongitude = 17.188459 },
            new TrackedObject {
                TrackedObjectId = "6",
                TrackedObjectName = "HUSLIK_03_Bubbub",
                TrackedObjectCode = "18026_2",
                DeviceCode = "ANITRA 026",
                TrackedFrom = "31.12.2018",
                SpeciesName_Scientific = "Bubo bubo",
                IndividualSex = "Female",
                CurrentAge = "+1K",
                LastPositionCountry = "CZ",
                LastPositionAdmin1 = "Býchory",
                LastPositionAdmin2 = "Kolín District",
                LastPositionSettlement = "Central Bohemian Region",
                LastPositionTime = "17.01.2019 08:39Z",
                LastPositionLatitude = 48.970139,
                LastPositionLongitude = 17.188459 },
        };

        private static List<Route> mockRoutes = new List<Route>
        {
            new Route
            {
                Id = 1,
                Name = "1",
                Note = "Some note",
                Locations = new List<Location>() {
                    new Location {Latitude = 50.053406,Longitude = 15.914619, Altitude = 190, Time = 1518248134},
                    new Location {Latitude = 50.056094,Longitude = 15.947017, Altitude = 190, Time = 1518248134},
                    new Location {Latitude = 50.05144,Longitude = 15.929835, Altitude = 190, Time = 1518262572},
                },
                Time = 1518262572,
                UserId = 1
            }
        };

    }
}
