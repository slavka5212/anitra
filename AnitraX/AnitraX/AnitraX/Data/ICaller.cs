﻿using AnitraX.Models;
using System.Collections.Generic;

namespace AnitraX.Data
{
    public interface ICaller
    {
        List<TrackedObject> GetTOList();
    }
}