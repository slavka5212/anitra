﻿using AnitraX.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Essentials;

namespace AnitraX.Data
{
    public class RestSharpCaller : ICaller
    {
        private RestClient client;
        public RestSharpCaller(string baseUrl)
        {
            client = new RestClient(baseUrl);
            string API_KEY = "f40139d03093c6fe1021a2f8e3ba67063ff6939f2beac5048d5f15a26437c965b3bc5339ca1d8e053a53a09ecbd312629c5ca93fc7f1cb195656f287fd2678b4";
            var authHeaderValue = API_KEY;
            client.AddDefaultHeader("Authorization", authHeaderValue);
        }

        public List<TrackedObject> TrackedObjects { get; private set; }
        public List<Route> Routes { get; private set; }

        public List<TrackedObject> GetTOList()
        {
            TrackedObjects = new List<TrackedObject>();

            var request = new RestRequest("v1/tracked-object/list", Method.GET);
            var response = client.Execute<List<TrackedObject>>(request);

            TrackedObjectManager.ListGetResponse listResponse =
                JsonConvert.DeserializeObject<TrackedObjectManager.ListGetResponse>(response.Content);
            try
            {
                if (listResponse.STATUS_CODE == "OK")
                {
                    TrackedObjects = listResponse.list;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
            return TrackedObjects;
        }

        public List<Route> GetRoutesList()
        {
            Routes = new List<Route>();

            var request = new RestRequest("v1/route", Method.GET);
            var response = client.Execute<List<Route>>(request);

            RouteManager.ListGetResponse listResponse =
                JsonConvert.DeserializeObject<RouteManager.ListGetResponse>(response.Content);
            try
            {
                if (listResponse.STATUS_CODE == "OK")
                {
                    Routes = listResponse.list;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
            return Routes;
        }

        public void SendRoutes()
        {
            var request = new RestRequest("v1/route/", Method.POST);

            List<Route> routes = App.DB.GetRoutesAsync().Result;
            
            // get device info
            // var device = DeviceInfo.Model;
            var deviceName = DeviceInfo.Name;

            var jsonObj = JsonConvert.SerializeObject(new { Routes = routes, Device = deviceName });
            Debug.Write(jsonObj);
            
            request.AddJsonBody(jsonObj);

            var response = client.Execute(request);
            RouteManager.ListGetResponse listResponse = JsonConvert.DeserializeObject<RouteManager.ListGetResponse>(response.Content);
            try
            {
                if (listResponse.STATUS_CODE == "OK")
                {
                    Debug.WriteLine("OK send routes");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"ERROR {0}", ex.Message);
            }
        }
    }
}
