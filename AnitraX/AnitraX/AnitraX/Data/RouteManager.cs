﻿using AnitraX.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Data
{
    /// <summary>
    /// RouteManager maps the object from the GET/POST request.
    /// </summary>
    public class RouteManager
    {
        public List<Route> TracksList;

        public RouteManager()
        {
            TracksList = new List<Route>();
        }
        
        public class ListGetResponse
        {
            public string STATUS_CODE { get; set; }
            public string MESSAGE { get; set; }
            [JsonProperty(PropertyName = "routes")]
            public List<Route> list { get; set; }
        }
    }
}
