﻿using AnitraX.Models;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AnitraX.Data
{
    public class Database
    {
        private readonly SQLiteAsyncConnection database;

        public Database(string databasePath)
        {
            this.database = new SQLiteAsyncConnection(databasePath);
            this.database.CreateTableAsync<Location>().Wait();
            this.database.CreateTableAsync<TrackedObject>().Wait();
            this.database.CreateTableAsync<Route>().Wait();
        }

        public async Task<List<TrackedObject>> GetTrackedObjectsAsync()
        {
            return await this.database.GetAllWithChildrenAsync<TrackedObject>(recursive: true);
        }

        public async Task<List<Route>> GetRoutesAsync()
        {
            return await this.database.GetAllWithChildrenAsync<Route>(recursive: true);
        }

        public async Task<List<Location>> GetLocationsAsync()
        {
            return await this.database.GetAllWithChildrenAsync<Location>(recursive: true);
        }

        public async Task<TrackedObject> GetTrackedObjectAsync(string toId)
        {
            return await this.database.GetWithChildrenAsync<TrackedObject>(toId, recursive: true);
        }

        public async Task<Route> GetRouteAsync(int routeId)
        {
            var queryResult = await this.database.Table<Route>().Where(d => d.Id == routeId).CountAsync();
            if (queryResult > 0)
            {
                return await this.database.GetWithChildrenAsync<Route>(routeId, recursive: true);
            }
            else
            {
                return null;
            }
        }

        public async Task<Location> GetLocationAsync(int locationId)
        {
            return await this.database.GetWithChildrenAsync<Location>(locationId, recursive: true);
        }
        public async Task AddRouteAsync(Route route)
        {
            await this.database.InsertWithChildrenAsync(route, recursive: true);
        }

        public async Task AddLocationsAsync(List<Location> locations)
        {
            await this.database.InsertAllAsync(locations);
        }

        public async Task StoreRouteAsync(Route route)
        {
            var foundItem = await this.GetRouteAsync(route.Id);
            if (foundItem != null)
            {
                await this.database.UpdateWithChildrenAsync(route);
            }
            else
            {
                await this.database.InsertWithChildrenAsync(route, recursive: true);
            }
        }

        public async Task StoreTOAsync(List<TrackedObject> trackedObjects)
        {
            await this.database.InsertWithChildrenAsync(trackedObjects, recursive: true);
        }
    }
}
