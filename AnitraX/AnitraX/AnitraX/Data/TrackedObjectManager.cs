﻿using AnitraX.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AnitraX.Data
{
    /// <summary>
    /// TrackedObjectManager maps the object from the GET request.
    /// </summary>
    public class TrackedObjectManager
    {
        
        public static string DATA = "data";
        public static string DATA_LAT = "lat";
        public static string DATA_LNG = "lng";
        public static string DATA_TIME = "time";


        public List<TrackedObject> TOlist;

        public TrackedObjectManager()
        {
            TOlist = new List<TrackedObject>();
        }
        

        public class ListGetResponse
        {
            public string STATUS_CODE { get; set; }
            public string MESSAGE { get; set; }
            [JsonProperty(PropertyName = "list")]
            public List<TrackedObject> list { get; set; }
        }


        public class TrackedObjectData
        {

        }

    }
}
