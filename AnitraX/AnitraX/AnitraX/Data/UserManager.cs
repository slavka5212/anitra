﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX.Data
{
    /// <summary>
    /// UserManager maps a Json object from the post response (login).
    /// </summary>
    class UserManager
    {
        public class UserPostResponse
        {
            public string STATUS_CODE { get; set; }
            public string MESSAGE { get; set; }
            public string API_KEY { get; set; }
            [JsonProperty(PropertyName = "id")]
            private int id { get; set; }
            [JsonProperty(PropertyName = "detail")]
            public Detail Detail { get; set; }
        }

        public class Detail
        {
            [JsonProperty(PropertyName = "FirstName")]
            public string FirstName { get; set; }
            [JsonProperty(PropertyName = "LastName")]
            public string LastName { get; set; }
            [JsonProperty(PropertyName = "Email")]
            public string Email { get; set; }
            [JsonProperty(PropertyName = "Role")]
            public string Role { get; set; } // "USER"
            [JsonProperty(PropertyName = "State")]
            public string State { get; set; } //  "us_active"
            [JsonProperty(PropertyName = "IsBlocked")]
            public bool IsBlocked { get; set; } // not blocked: 0
        }
    }
}
