﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnitraX
{
    class Constants
    {
        // Credentials that are hard coded into the REST service
        public static string Email = "slavka.ivanicova@gmail.com";
        public static string Password = "memos123";

        // DateTime format
        public static string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";

        public static int MinSecGPSdataCollection = 5;
    }
}
