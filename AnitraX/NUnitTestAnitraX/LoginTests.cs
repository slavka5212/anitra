using NUnit.Framework;

namespace LoginTests
{
    public class LoginTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void LoginFalseTest1()
        {
            var loginPage = new AnitraX.Views.LoginPage();
            bool loggedin = loginPage.Login(null, null);

            Assert.IsFalse(loggedin);
        }
        
    }
}